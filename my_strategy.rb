require './model/action_type'
require './model/game'
require './model/move'
require './model/hockeyist'
require './model/world'

Unit.class_eval do
  attr_writer :x, :y

  def speed
    Math::sqrt(speed_x**2 + speed_y**2)
  end

  def speed_angle
    if speed > 0
      res = Math::asin(speed_y/speed)
      # Here is a some magic (angle calculation)
      if speed_x < 0
        speed_y > 0 ? Math::PI - 2*res : -(Math::PI + 2*res)
      else
        speed_y > 0 ? C::PI_2 - 2*res : -(C::PI_2 + 2*res)
      end
    else
      "No speed"
    end
  end

  def self.simple(x, y)
    Unit.new(0, 0, 0, x, y, 0, 0, 0, 0)
  end
end

module C
  STRIKE_MAX_DISTANSE = 450
  STRIKE_MIN_DISTANSE = 250
  FALSE_STRIKE_MAX_DISTANSE = 550
  ACTIVE_DEFENCE_DISTANCE = 400
  MAX_HOCKEYIST_SPEED = 10

  PI_2 = Math::PI/2
end

module FriendOrFoe
  @@my_forward_id = nil
  @@my_defencer_id = nil
  @@actions = {}

  def clear_roles_if_any_goal
    if @my_player.just_scored_goal || @enemy.just_scored_goal
      @@my_forward_id = nil
      @@my_defencer_id = nil
      @@actions.clear
    end
  end

  def update_friends_and_foes
    @@teammates = @world.hockeyists.select { |h| h.teammate }
    @@enemies = @world.hockeyists.select { |h| !h.teammate }

    @@teammates_in_field = @@teammates.select { |h| h.type != HockeyistType::GOALIE }
    @@enemies_in_field = @@enemies.select { |h| h.type != HockeyistType::GOALIE }

    goalie = @@teammates.find { |h| h.type == HockeyistType::GOALIE }
    @@my_goalie = goalie if goalie

    goalie = @@enemies.find { |h| h.type == HockeyistType::GOALIE }
    @@enemy_goalie = goalie if goalie

    if !@my_player.just_scored_goal && !@enemy.just_scored_goal
      @@my_forward_id = @@teammates_in_field.min_by { |h| (h.x - @world.width/2).abs }.id unless @@my_forward_id
      @@my_defencer_id = @@teammates_in_field.reject { |h| h.id == @@my_forward_id }.min_by { |h| (h.x - @@my_goalie.x).abs }.id unless @@my_defencer_id
    end
  end

  def is_forward(hockeyist)
    @@my_forward_id == hockeyist.id
  end

  def is_defencer(hockeyist)
    @@my_defencer_id == hockeyist.id
  end

  def has_enemy_net_defencer(distance = 160)
    net_center = [@enemy.net_front, (@enemy.net_top + @enemy.net_bottom)/2]
    @@enemies_in_field.any? { |e| e.get_distance_to(*net_center) < distance }
  end

  def get_defencer(me)
    if @@my_defencer_id
      @@teammates_in_field.find { |t| t.id == @@my_defencer_id }
    else
      @@teammates_in_field.reject { |t| t.id == me.id }.first
    end
  end

  def get_forward(me)
    if @@my_forward_id
      @@teammates_in_field.find { |t| t.id == @@my_forward_id }
    else
      @@teammates_in_field.reject { |t| t.id == me.id }.first
    end
  end

  def set_forward(hockeyist)
    @@my_forward_id = hockeyist.id
  end

  def is_not_teammate(id)
    @@teammates_in_field.all? { |t| t.id != id }
  end

  def is_enemy_attacker(id)
    @@enemies_in_field.any? { |e| e.id == id }
  end

  def swap_forward_defencer
    @@my_forward_id, @@my_defencer_id = @@my_defencer_id, @@my_forward_id
    @@actions.delete(@@my_forward_id)
    @@actions.delete(@@my_defencer_id)
  end

  def nearest_enemy(hockeyist)
    res = block_given? ? @@enemies_in_field.select { |e| yield(e) } : @@enemies_in_field
    res.min_by { |enemy| hockeyist.get_distance_to_unit(enemy) }
  end

  def nearest_enemies(hockeyist, distance)
    c1 = @@enemies_in_field.select { |e| hockeyist.get_distance_to_unit(e) <= distance }
    c2 = @@enemies_in_field.select { |e| hockeyist.get_distance_to_unit(e) > distance && hockeyist.get_distance_to_unit(e) <= 1.5*distance }
    c1 + c2
  end

  def nearest_enemies_count(hockeyist, distance)
    nearest_enemies(hockeyist, distance).size
  end

  def me_blocked_for_goal(hockeyist, nearest_enemies)
    if nearest_enemies.size > 0
      hd = hockeyist.get_distance_to_unit(@@enemy_goalie)
      ed = nearest_enemies.map { |e| e.get_distance_to_unit(@@enemy_goalie) }.reduce(:+)/nearest_enemies.size
      hd > ed
    else
      false
    end
  end

  def all_enemies_far_than(hockeyist, distance)
    @@enemies_in_field.all? { |e| hockeyist.get_distance_to_unit(e) > distance }
  end

  def all_enemies_y_pos(hockeyist)
    middle_percent = 0.5
    middle_y = (@my_player.net_top + @my_player.net_bottom)/2

    net_height = @my_player.net_bottom - @my_player.net_top
    delta_middle = (net_height * middle_percent)/2
    top_middle = middle_y - delta_middle
    bottom_middle = middle_y + delta_middle

    enemies = if @enemy.net_front < @my_player.net_front
      @@enemies_in_field.reject { |e| e.x > hockeyist.x }
    else
      @@enemies_in_field.reject { |e| e.x < hockeyist.x }
    end

    enemies = enemies.reject { |e| in_rect_y(e, top_middle, bottom_middle) }

    closest_enemies = enemies.count { |e| hockeyist.get_distance_to_unit(e) <= e.radius*5 }

    if closest_enemies >= 2
      hockeyist.y < middle_y ? :bottom : :top
    else
      if enemies.any? && enemies.all? { |e| e.y <= top_middle }
        :top
      elsif enemies.any? && enemies.all? { |e| e.y >= bottom_middle }
        :bottom
      else
        :middle
      end
    end
  end

  def in_rect(hockeyist, left, rigth, top, bottom)
    in_rect_x(hockeyist, left, rigth) && in_rect_y(hockeyist, top, bottom)
  end

  def in_rect_x(hockeyist, left, rigth)
    hockeyist.x >= left && hockeyist.x <= rigth
  end

  def in_rect_y(hockeyist, top, bottom)
    hockeyist.y >= top && hockeyist.y <= bottom
  end
end

module Memory
  @@tick = -1

  def update_memory
    if @@tick != @world.tick
      @@tick = @world.tick
      yield if block_given?
    end
  end
end

module Angles
  DELTA_Y = 6*20
  NET_BLOCK_AREA = 10

  def get_possible_turn_angle(angle)
    if angle > 0
      angle > @game.hockeyist_turn_angle_factor ? @game.hockeyist_turn_angle_factor : angle
    else
      angle < -@game.hockeyist_turn_angle_factor ? -@game.hockeyist_turn_angle_factor : angle
    end
  end

  def get_lead(hockeyist, friction = 1.0)
    ticks = hockeyist.state == HockeyistState::SWINGING ? @game.max_effective_swing_ticks - hockeyist.swing_ticks : @game.max_effective_swing_ticks
    dx = hockeyist.speed_x * ticks * friction
    dy = hockeyist.speed_y * ticks * friction
    [dx, dy]
  end

  def is_ready_for_strike(hockeyist, goal, precision)
    speed = hockeyist.speed

    angle_ready = hockeyist.get_angle_to(*goal).abs < precision
    speed_ready =
      if speed > 0
        speed_angle = hockeyist.speed_angle
        goal_angle = hockeyist.angle + hockeyist.get_angle_to(*goal)
        #$stdout.puts "speed_angle=#{speed_angle*180/Math::PI} goal_angle=#{goal_angle*180/Math::PI} as=#{hockeyist.angular_speed}"
        diff = (speed_angle - goal_angle).abs % Math::PI
        diff < precision
      else
        true
      end

    angle_ready && speed_ready
  end

  def get_strike_point(hockeyist)
    distance = hockeyist.get_distance_to_unit(@enemy_goalie)

    lead =
      if @actions[hockeyist.id] == :go_back || distance > C::FALSE_STRIKE_MAX_DISTANSE + 2*hockeyist.radius
        [0, 0]
      else
        get_lead(hockeyist, 0.25)
      end

    puck = [hockeyist.x, hockeyist.y]
    #puck = [hockeyist.x + lead[0], hockeyist.y + lead[1]]

    middle_x = @world.width/2
    middle_y = (@my_player.net_top + @my_player.net_bottom)/2

    top_edge = @enemy.net_top - DELTA_Y
    bottom_edge = @enemy.net_bottom + DELTA_Y

    top = get_strike_angle_inner([@enemy.net_front, @enemy.net_top], puck, @puck.radius, middle_y)
    bottom = get_strike_angle_inner([@enemy.net_front, @enemy.net_bottom], puck, @puck.radius, middle_y)

    res = if hockeyist.y < middle_y
      [@enemy.net_front, bottom[1], true]
    else
      [@enemy.net_front, top[1], true]
    end

    if distance < C::STRIKE_MIN_DISTANSE && hockeyist.state != HockeyistState::SWINGING
      @actions[hockeyist.id] = :go_back
    elsif distance > C::FALSE_STRIKE_MAX_DISTANSE && @actions[hockeyist.id] == :go_back
      @actions.delete(hockeyist.id)
    end

    go_back = @actions[hockeyist.id] == :go_back

    if distance > C::FALSE_STRIKE_MAX_DISTANSE || go_back
      res[0] = correct_x(hockeyist, go_back)

      enemy_y_pos = all_enemies_y_pos(hockeyist)
      res[1] =
        case
        when @actions[hockeyist.id] == :go_bottom then bottom_edge
        when @actions[hockeyist.id] == :go_top then top_edge
        when enemy_y_pos == :top then bottom_edge
        when enemy_y_pos == :bottom then top_edge
        else correct_y(hockeyist, middle_y, go_back, top_edge, bottom_edge)
        end

      res[2] = false
    elsif hockeyist.y > top_edge && hockeyist.y < bottom_edge && distance > middle_x
      # if hockeyist in the y-middle go to edge
      res[0] = correct_x(hockeyist, go_back)
      res[1] = correct_y(hockeyist, middle_y, go_back, top_edge, bottom_edge)
      res[2] = false
    end

    unless @actions[hockeyist.id]
      @actions[hockeyist.id] = res[1] < middle_y ? :go_top : :go_bottom
    end

    res
  end

  def correct_x(hockeyist, go_back, inverse = false)
    coeff = go_back ? -1 : 1
    coeff *= -1 if inverse

    if hockeyist.x > @enemy_goalie.x
      hockeyist.x - 100*coeff
    else
      hockeyist.x + 100*coeff
    end
  end

  def correct_y(hockeyist, middle_y, go_back, top_edge, bottom_edge)
    if hockeyist.y < middle_y
      go_back ? bottom_edge : top_edge
    else
      go_back ? top_edge : bottom_edge
    end
  end

  def get_strike_angle_inner(net_top_edge, puck, puck_radius, middle_y)
    net_puck_projection = [net_top_edge[0], puck[1]]

    h = get_distance(puck, net_top_edge)
    h = puck_radius if h < puck_radius
    t = Math::sqrt(h**2 - puck_radius**2)
    puck_x_distance = (@puck.x - net_top_edge[0]).abs #get_distance(net_puck_projection, puck)
    top_edge = net_top_edge[1] < middle_y

    middle = @enemy.net_top <= puck[1] - puck_radius && @enemy.net_bottom >= puck[1] + puck_radius
    top = !middle && @enemy.net_top > puck[1] - puck_radius
    bottom = !middle && !top

    dir = if middle
      top_edge ? -1 : 1
    else
      top ? 1 : -1
    end

    # gamma = Math::asin(get_distance(net_top_edge, net_puck_projection)/h)
    # beta = Math::asin(puck_radius/h)
    gamma = Math::atan(get_distance(net_top_edge, net_puck_projection)/puck_x_distance)
    beta = Math::atan(puck_radius/t)

    alfa = if middle
      gamma - beta
    elsif top
      top_edge ? gamma + beta : gamma - beta
    else
      top_edge ? gamma - beta : gamma + beta
    end
    a = Math.tan(alfa)*puck_x_distance*dir

    target_y = net_puck_projection[1] + a
    pos = if middle
      :middle
    elsif top
      :top
    else
      :bottom
    end

    [alfa, target_y, pos]
  end

  def get_distance(p1, p2)
    Math::sqrt((p1[0] - p2[0])**2 + (p1[1] - p2[1])**2)
  end

  def get_defence_point(hockeyist)
    enemy = @enemies_in_field.find { |e| e.id == @puck.owner_hockeyist_id }
    if enemy && enemy.get_distance_to_unit(@my_goalie) < C::ACTIVE_DEFENCE_DISTANCE
      get_active_defence_point(hockeyist, enemy)
    else
      get_defence_point_before_net
    end
  end

  def get_defence_point_before_net
    # top_window = @my_goalie.y - @my_player.net_top
    # bottom_window = @my_player.net_bottom - @my_goalie.y
    # y = if top_window > bottom_window
    #   @my_goalie.y - top_window/2
    # else
    #   @my_goalie.y + bottom_window/2
    # end
    y = (@my_player.net_top + @my_player.net_bottom)/2
    x = if @my_goalie.x < @world.width / 2
      @my_goalie.x + @my_goalie.radius * 2
    else
      @my_goalie.x - @my_goalie.radius * 2
    end
    [x, y, false]
  end

  def get_active_defence_point(hockeyist, enemy)
    time_step = 50 / [hockeyist.speed, 1].max
    point = Unit.simple(@puck.x, @puck.y)

    best_distance = hockeyist.get_distance_to_unit(point)
    best_point = point

    time = 0
    while in_field(point)
      point.x = point.x + enemy.speed_x*time_step
      point.y = point.y + enemy.speed_y*time_step

      distance = (hockeyist.get_distance_to_unit(point) - time*C::MAX_HOCKEYIST_SPEED).abs
      if distance < best_distance
        best_distance = distance
        best_point = point
      end

      time += time_step
    end
    [point.x, point.y, true]
  end

  def in_field(unit)
    unit.x > 0 && unit.x < @world.width && unit.y > 0 && unit.y < @world.height
  end

  def go_straight_or_back(angle_diff, hockeyist, distance = 0, smoothly = false, only_straight = false)
    grad = angle_diff.abs*180/Math::PI
    speed_cos = (grad < 90 ? grad : 180 - grad) < 7 ? 1 : 0

    res = if grad < 90 || only_straight
      [angle_diff, 1*speed_cos, false]
    else
      [-angle_diff, -1*speed_cos, false]
    end

    if smoothly
      speed = hockeyist.speed

      a = speed*speed/(2*distance)
      if a >= @game.hockeyist_speed_down_factor*0.6
        res[1] *= -1
        res[2] = true
      end
    end
    res
  end
end

module Speeds
  def can_defencer_take_puck(defencer, puck, probability = 1.0)
    a = (@game.pick_up_puck_base_chance - 1.6)/@game.struck_puck_initial_speed_factor
    max_catchable_puck_speed = (probability - 1.6)/a

    # a_strike = (@game.strike_puck_base_chance - 1.75)/@game.struck_puck_initial_speed_factor
    # max_catchable_strike_speed = (probability - 1.75)/a_strike
    # strike_probability = a_strike*max_catchable_puck_speed + 1.75

    #puts "max = #{max_catchable_puck_speed} speed=#{puck.speed} for probability #{(probability*100).to_i}% ss=#{max_catchable_strike_speed} sp=#{strike_probability}"
    puck.speed < max_catchable_puck_speed
  end
end

module PathFinder
  class Matrix
    attr_reader :row_size, :col_size

    def initialize(rows, cols)
      @row_size = rows
      @col_size = cols

      @m = Array.new(rows) do |row|
        Array.new(cols) do |col|
          yield row, col
        end
      end
    end

    def each_with_index
      @m.each_with_index do |row, row_index|
        row.each_with_index do |e, col_index|
          yield e, row_index, col_index
        end
      end
    end

    def [](row, col)
      @m[row][col]
    end

    def []=(row, col, value)
      @m[row][col] = value
    end

    def row(row)
      @m[row]
    end

    def clear
      @m.each_with_index do |row, row_index|
        row.each_with_index do |e, col_index|
          row[col_index] = yield row_index, col_index
        end
      end
      self
    end
  end

  SCALE = 10
  SKIN_THINKNESS = 3

  def init_bar_pattern(radius)
    half_scale = SCALE/2.0
    radius_2 = 2 * radius
    size = 4 * radius / SCALE

    @@bar_pattern ||= Matrix.new(size, size) do |row, col|
      distance = get_distance_4(col*SCALE + half_scale, row*SCALE + half_scale, radius_2, radius_2)
      distance > radius_2
    end
  end

  def init_area(width, height, bars = [])
    left = @game.goal_net_width / SCALE
    rigth = (@world.width - @game.goal_net_width) / SCALE

    @row_count = (height / SCALE).round
    @col_count = (width / SCALE).round
    @area = if @area
      @area.clear do |row, col|
        col > left && col < rigth
      end
    else
       Matrix.new(@row_count, @col_count) do |row, col|
        col > left && col < rigth
      end
    end

    bars.each do |bar|
      xc = bar[0]/SCALE
      yc = bar[1]/SCALE

      half = @@bar_pattern.row_size / 2

      @@bar_pattern.each_with_index do |e, row, col|
        x = xc - half + col
        y = yc - half + row
        if 0 <= x && x < @col_count && 0 <= y && y < @row_count
          @area[y, x] &&= e
        end
      end
    end
  end

  def find_way(from, to)
    from = [from[0] / SCALE, from[1] / SCALE]
    to = [to[0] / SCALE, to[1] / SCALE]
    res = do_find_path(from, to)
    res.each { |point| point[0] *= SCALE; point[1] *= SCALE } if res
  end

  def find_first_way_point(from, to)
    from_2 = correct_target_by_y([(from.x / SCALE).round, (from.y / SCALE).round])
    to_2 = correct_target_by_y([(to[0] / SCALE).round, (to[1] / SCALE).round])
    from = [from.x, from.y]
    res = do_find_path(from_2, to_2)
    #debug_path(from, to, res)
    if res && res.any?
      point = res.first
      point[0] *= SCALE
      #if (from[0] - @my_goalie.x).abs < SKIN_THINKNESS && (from[1] - @my_goalie.y).abs < 2*@my_goalie.radius + SKIN_THINKNESS
      if get_distance_4(from[0], from[1], @my_goalie.x, @my_goalie.y) < 2*@my_goalie.radius + SKIN_THINKNESS
        if from[0] > @world.width / 2
          point[0] -= SCALE / 2.0
        else
          point[0] += SCALE / 2.0
        end
      else
        point[0] += SCALE / 2.0 if from[0] + SKIN_THINKNESS < to[0]
        point[0] -= SCALE / 2.0 if from[0] > to[0] + SKIN_THINKNESS
      end

      point[1] *= SCALE
      point[1] += SCALE / 2.0 if from[1] + SKIN_THINKNESS < to[1]
      point[1] -= SCALE / 2.0 if from[1] > to[1] + SKIN_THINKNESS
      point
    end
  end

  def correct_target_by_y(to)
    targets = [to, [to[0], to[1]-1], [to[0], to[1]+1]]
    while targets.any?
      p = targets.shift
      return p if @area[p[1], p[0]]
      if p[1] < to[1] && p[1]-1 >= 0
        targets << [p[0], p[1]-1]
      elsif p[1] > to[1] && p[1]+1 < @area.row_size
        targets << [p[0], p[1]+1]
      end
    end
  end

  def debug_path(from, to, res)
    @area.row_size.times do |row|
      @area.row(row).each_with_index do |e, col|
        if res && res.include?([col, row])
          print '+'
        elsif [col, row] == from
          print '@'
        elsif [col, row] == to
          print '*'
        else
          print (e ? '_' : '0')
        end
      end
      puts
    end
  end

  private

  def get_distance_4(x1, y1, x2, y2)
    Math::sqrt((x1 - x2)**2 + (y1 - y2)**2)
  end

  def do_find_path(from, to)
    been_there = {}
    pqueue = PriorityQueue.new
    pqueue << [1, [from, [], 1]]
    while !pqueue.empty?
      spot, path_so_far, cost_so_far = pqueue.next
      next if been_there[spot]
      newpath = [path_so_far, spot]
      if spot == to
        @path = []
        newpath.flatten.each_slice(2) { |i, j| @path << [i,  j] }
        return @path
      end
      been_there[spot] = true
      spotsfrom(spot).each do |newspot|
        next if been_there[newspot]
        newcost = cost_so_far + 1
        pqueue << [newcost + estimate(newspot, to), [newspot, newpath, newcost]]
      end
    end
    return nil
  end

  def estimate(spot, goal)
    (spot[0] - goal[0]).abs + (spot[1] - goal[1]).abs
  end

  def spotsfrom(spot)
    res = []
    x_deltas = [0, 1]
    y_deltas = [0, 1]
    x_deltas << -1 if spot[0] > 0
    y_deltas << -1 if spot[1] > 0
    x_deltas.each do |x|
      y_deltas.each do |y|
        if x != 0 || y != 0
          if spot[0] + x >= 0 && spot[0] + x < @col_count && spot[1] + y >= 0 && spot[1] + y < @row_count
            point = [spot[0] + x, spot[1] + y]
            res << point if @area[point[1], point[0]]
          end
        end
      end
    end
    res
  end

  # I suppose someone would think I should use a heap here.
  # I've found that the built-in sort method is much faster
  # than any heap implementation in ruby.  As a plus, the logic
  # is easier to follow.
  class PriorityQueue
    def initialize
      @list = []
    end

    def add(priority, item)
      # Add @list.length so that sort is always using Fixnum comparisons,
      # which should be fast, rather than whatever is comparison on `item'
      @list << [priority, @list.length, item]
      #@list.sort!
      self
    end

    def <<(pritem)
      add(*pritem)
    end

    def next
      min = @list.first
      index = 0
      @list.each_with_index do |e, i|
        if e[0] < min[0]
          min = e
          index = i
        end
      end
      @list.delete_at(index)
      min[2]
    end

    def empty?
      @list.empty?
    end
  end
end

class MyStrategy
  STRIKE_ANGLE = 1*Math::PI/180
  PASS_ANGLE = 4*Math::PI/180
  PASS_ACCEPT_ANGLE = 45*Math::PI/180
  DEFENCER_GO_ANGLE = 0.7

  include FriendOrFoe
  include Memory
  include Angles
  include PathFinder
  include Speeds

  def pass_from(me, move)
    defencer = get_defencer(me)
    angle = me.get_angle_to_unit(defencer)
    distance = me.get_distance_to_unit(defencer)
    move.turn, move.speed_up, stopping = go_straight_or_back(angle, me, distance, true, true)
    if angle.abs < PASS_ANGLE
      move.action = ActionType::PASS
      move.pass_power = [[1.7*distance/@world.width, 1.0].min, 0.75].max
      move.pass_angle = -me.angular_speed
      @actions[me.id] = nil
    end
  end

  # @param [Hockeyist] me
  # @param [World] world
  # @param [Game] game
  # @param [Move] move
  def move(me, world, game, move)
    @game = game
    @world = world
    @my_player = world.get_my_player
    @enemy = world.get_opponent_player
    @puck = world.puck
    @actions = @@actions
    width = world.width

    update_memory do
      update_friends_and_foes
      #init_bar_pattern(@@my_goalie.radius)
      #@area = nil
    end

    @enemy_goalie = @@enemy_goalie
    @my_goalie = @@my_goalie
    @enemies_in_field = @@enemies_in_field

    #init_area(world.width, world.height, [[@my_goalie.x, @my_goalie.y]])

    if @puck.owner_hockeyist_id != me.id && me.state == HockeyistState::SWINGING
      move.action = ActionType::CANCEL_STRIKE
      return
    end

    if @puck.owner_hockeyist_id != me.id && @actions[me.id] == :pass
      @actions[me.id] = nil
    end

    @@puck_max_speed ||= 0
    @@puck_max_speed = [@@puck_max_speed, @puck.speed].max
    #puts "puck speed=#{@puck.speed} max=#{@@puck_max_speed}"

    if is_forward(me)
      #puts "speed_angle=#{me.speed_angle*180/Math::PI}" if me.speed > 0
      if @puck.owner_hockeyist_id == me.id
        *goal, real_goal = get_strike_point(me)
        goal_angle = me.get_angle_to(*goal)
        distance = me.get_distance_to_unit(@@enemy_goalie)
        if @actions[me.id] == :go_back || @actions[me.id] == :pass
          if me.state == HockeyistState::SWINGING
            move.action = ActionType::CANCEL_STRIKE
          else
            pass_from(me, move)
          end
        elsif me.state == HockeyistState::SWINGING
          if me.swing_ticks >= @game.max_effective_swing_ticks
            move.action = ActionType::STRIKE
          else
            move.action = ActionType::SWING
          end
        elsif distance < C::FALSE_STRIKE_MAX_DISTANSE && distance > C::STRIKE_MIN_DISTANSE\
            && real_goal && goal_angle.abs <= STRIKE_ANGLE
          move.speed_up = 1.0
          move.turn = goal_angle

          if has_enemy_net_defencer
            if is_ready_for_strike(me, goal, 2*STRIKE_ANGLE)
              move.action = ActionType::SWING
              @actions[me.id] = :swing
            end
          elsif distance < C::STRIKE_MAX_DISTANSE
            move.action = ActionType::PASS
            move.pass_power = 1.0
            move.pass_angle = -me.angular_speed
          end
        else
          move.turn = goal_angle
          move.speed_up = 1.0

          defencer = get_defencer(me)
          nearest = nearest_enemies(me, me.radius*2.1)
          if nearest.size >= 2 && nearest_enemies_count(defencer, me.radius*2.5) <= 1 \
            && me_blocked_for_goal(me, nearest) && defencer.get_angle_to_unit(@puck) <= PASS_ACCEPT_ANGLE
            @actions[me.id] = :pass
            pass_from(me, move)
          end
        end
        #puts "me action = #{@actions[me.id]}"
      else
        @actions[me.id] = nil
        move.turn = get_possible_turn_angle(me.get_angle_to_unit(@puck))
        move.speed_up = 1.0

        if me.get_distance_to_unit(@puck) <= game.stick_length
          move.action = ActionType::TAKE_PUCK
        else
          enemy = nearest_enemy(me)
          if enemy.id == @puck.owner_hockeyist_id && me.get_distance_to_unit(enemy) <= game.stick_length
            move.action = ActionType::STRIKE
          end
        end
      end
    elsif is_defencer(me)
      *point, active = get_defence_point(me)
      distance = me.get_distance_to(*point)
      angle = me.get_angle_to(*point)
      *go, stopping = go_straight_or_back(angle, me, distance, true, distance > 150 || active)
      if distance > me.radius
        #ok
      elsif distance < 0.5*me.radius
        go[0] = me.get_angle_to_unit(@puck)
        go[1] = 0
      elsif go[1] == 0
        go[0] = me.get_angle_to_unit(@puck)
      else
        go[0] = me.get_angle_to_unit(@puck)
        go[1] /= 2 unless stopping
      end
      move.turn, move.speed_up = *go

      if me.get_distance_to_unit(@puck) < game.stick_length
        if is_enemy_attacker(@puck.owner_hockeyist_id)
          move.action = ActionType::STRIKE
        elsif is_not_teammate(@puck.owner_hockeyist_id)
          #if can_defencer_take_puck(me, @puck, 0.60)
            move.action = ActionType::TAKE_PUCK
          # else
          #   move.action = ActionType::STRIKE
          # end
        end
      end

      forward_to_puck = @puck.get_distance_to_unit(get_forward(me))
      defencer_to_puck = @puck.get_distance_to_unit(me)
      puck_rate = forward_to_puck/defencer_to_puck
      all_enemies_far = all_enemies_far_than(me, 0.6*world.width)

      if @puck.owner_hockeyist_id == me.id || (puck_rate > 5 && all_enemies_far)
        swap_forward_defencer
      end
    end
    clear_roles_if_any_goal
  end
end